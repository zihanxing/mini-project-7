# Enhanced Data Management with Qdrant Vector Database

This comprehensive project is centered around the advanced manipulation and retrieval of data via the Qdrant vector search engine, a sophisticated platform designed for efficient data handling. The initiative encompasses a broad spectrum of functionalities including data insertion, querying, visualization, and summarization within the dynamic environment of Qdrant collections. A significant aspect of this project involves employing Docker to initiate a Qdrant container, meticulously configuring it to align with specific port mappings, inclusive of the gRPC port setup. The core of this project lies in the implementation of a vector database system, capable of processing vectors comprising numerical values and associated subject identifiers, subsequently generating refined results post application of various filters.

## Objectives
- Efficient ingestion of data into the Vector database.
- Execution of comprehensive queries and data aggregations.
- Advanced visualization of query outputs.

## Implementation Process
### Initial Setup: Launching the Vector Database
- **Acquire Qdrant Vector DB**:
    ```bash
    docker pull qdrant/qdrant
    ```
- **Activate Qdrant Instance**:
    ```bash
    docker run -p 6334:6334 qdrant/qdrant
    ```
### Configuration and Development
- **Dependency Management**: Ensure all necessary dependencies are specified within the `Cargo.toml` file.
- **Codebase Setup**: Develop the required functionalities within the `main.rs` file.

### Operational Breakdown
1. **Configuration and Initialization**: Initiates with establishing a local Qdrant server connection (`http://localhost:6334`), followed by configuring a Qdrant client.
   
2. **Collection Creation**: A new collection, `mini_project_7`, is crafted within the Qdrant database, designed to hold vectors in a 10-dimensional vector space, utilizing the Cosine distance metric for similarity assessments.

3. **Vector Generation and Statistical Analysis**: Generates a set of random 3-dimensional vectors (`num_vectors = 3`), subsequently computing their statistical metrics (minimum, median, and maximum values) which are then cataloged against unique identifiers within a hashmap.

4. **Data Insertion into Collection**: Iteratively processes each vector, assembling a payload from its statistical metrics, and integrates this information into the designated Qdrant collection.

5. **Data Querying and Result Presentation**: Executes a search query within the collection post data insertion, employing a predefined query vector to fetch and display the closest matching points based on the configured distance metric.

6. **Asynchronous Programming and Error Management**: Utilizes Rust's `async/await` syntax for efficient non-blocking operations and incorporates the `anyhow` crate for streamlined error handling.

### Tools and Libraries Utilized
- **`qdrant_client`**: For Qdrant database interactions.
- **`rand`**: For random number generation in vector creation.
- **`serde_json`**: For JSON data structure management.
- **`anyhow`**: For simplified error handling.

This project lays down a foundational framework for vector database utilization in Qdrant, facilitating data insertion, search operations, and result visualization, paving the way for more intricate implementations such as recommendation engines and nearest neighbor searches in expansive dimensional spaces.

### Execution and Verification
- Navigate to the project directory and execute:
    ```bash
    cargo run
    ```

## Outcome Visualization
The results and visual representations can be observed in the following figures:
![Docker Setup](./docker.png)
![Query Results](./result.png)
